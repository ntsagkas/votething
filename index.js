const app = require("express")();
const httpServer = require("http").createServer(app);
const options = {  cors: {
    origin: '*',
  }};
const io = require("socket.io")(httpServer, options);



let optionOne = 1;
let optionTwo = 1;

//listen for socket.io connections
io.on("connection", socket => { 
    //emit a custom event to the client
    socket.emit("pongServer", "Hello there from the server");
    socket.emit("pongVoteResult", {optionOne: optionOne, optionTwo: optionTwo});

    
    
    //listen for messages from the client
    socket.on("clientMessage", (data) => {
        console.log(data);
        socket.emit("pongServer", "PONG");
    });


    socket.on("voteOptionOne", (data) => {
        optionOne++;
        socket.emit("pongVoteResult", {optionOne: optionOne, optionTwo: optionTwo});
    });

    socket.on("voteOptionTwo", (data) => {
        optionTwo++;
        socket.emit("pongVoteResult", {optionOne: optionOne, optionTwo: optionTwo});
    });



 });






httpServer.listen(3000);

