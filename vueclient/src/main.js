import { createApp } from 'vue'
import App from './App.vue'

import VueSocketIO from 'vue-3-socket.io'
import SocketIO from 'socket.io-client'

const socketio = new VueSocketIO({
    debug: true,
    connection: SocketIO('http://localhost:3000')
});


import './assets/main.css'

createApp(App).use(socketio).mount('#app')
